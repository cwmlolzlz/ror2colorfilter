# Creating your own color filter

**Requirements:**
* BepInEx installed
* A C# project with the following 

In your project you will need 
* A C# project setup for development
* The latest version of the ColorFilter plugin included in the project.

If this is your first time modding Risk of Rain 2 I would highly recommend reading the [\[In depth\] First mod](https://github.com/risk-of-thunder/R2Wiki/wiki/In-depth-First-mod) guide on the R2API wiki.
If you can follow the guide you should have an easy time making a filter.

This guide will be using example code that applies grayscale to the game.

## Step 1 - Basic code

The basis of your plugin is going to begin the same as pretty much all RoR2 plugins.
Begin with the following code, you may already have this if you have followed guides.

```C#
using BepInEx;
using ColorFilter;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace GrayscaleColorFilter {
    [BepInDependency ("com.bepis.r2api")]
    [BepInPlugin ("com.yourname.grayscale", "GrayscaleColorFilter", "1.0.0")]
    public class GrayscaleColorFilterPlugin: BaseUnityPlugin {
        public void Awake() {

        }
    }
}
```

## Step 2 - Notifying the filter loader

The most essential step is to notify the ColorFilterLoad to load the filters from your plugin.
Generally, `ColorFilterLoader.LoadFiltersWhenReady()` should be called in your plugin's `Awake()` method.

You only need to call it once.

```C#
using BepInEx;
using ColorFilter;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace GrayscaleColorFilter {
    [BepInDependency ("com.bepis.r2api")]
    [BepInPlugin ("com.yourname.grayscale", "GrayscaleColorFilter", "1.0.0")]
    public class GrayscaleColorFilterPlugin: BaseUnityPlugin {
        public void Awake() {
            ColorFilterLoader.LoadFiltersWhenReady();
        }
    }
}
```

## Step 3 - Creating the filter

Every filter consists of three parts that will define your filter and its effect.

* `optionName` - The filter's display name
* `convarValue` - The filter's value it is known as. This should be unique for your filter otherwise it may not load.
* `ApplyChanges(ColorGrading settings)` - The filter's effect

```C#
using BepInEx;
using ColorFilter;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace GrayscaleColorFilter {

    [BepInDependency ("com.bepis.r2api")]
    [BepInPlugin ("com.yourname.grayscale", "GrayscaleColorFilter", "1.0.0")]
    public class GrayscaleColorFilterPlugin: BaseUnityPlugin {
        public void Awake() {
            ColorFilterLoader.LoadFiltersWhenReady();
        }
    }

    public class GrayscaleColorFilter: ColorFilterEffect {
        public void Awake() {
            this.optionName = "Grayscale"; // this filter will show as "Custom Noir" in the settings
            this.convarValue = "grayscale"; // this is hidden from the user and must be unique to your filter.
        }

        public override void ApplyChanges(ColorGrading settings) {
            settings.saturation.Override(-100); // remove all color from the screen
        }
    }
}
```

The filter can be customised as far as Unity's [ColorGrading](https://docs.unity3d.com/Packages/com.unity.postprocessing@2.1/manual/Color-Grading.html) options go.

Note: Remember to use `Override()` when setting a parameter on the ColorGrading object. This will ensure it will override any existing color grading in the game.

You can subclass `ColorFilterOption` as many times as you want in your project.
Your plugin essentially functions as a filter collection.
