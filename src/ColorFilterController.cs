﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace ColorFilter {
    class ColorFilterController: MonoBehaviour {

        public PostProcessProfile profile;

        public ColorGrading baseSettings;
        public ColorFilterEffect currentFilter;

        private void Start() {
            this.baseSettings = profile.GetSetting<ColorGrading>() ?? ScriptableObject.CreateInstance<ColorGrading>();            
            ApplyFilter(ColorFilterLoader.GetOptionForValue("off"));
        } 

        private void Update() {
            if(currentFilter.convarValue != ColorFilterConVar.instance.value) {
                ColorFilterEffect newFilter = ColorFilterLoader.GetOptionForValue(ColorFilterConVar.instance.value);
                ApplyFilter(newFilter);
            }
        }

        private void ApplyFilter(ColorFilterEffect filter) {
            if(filter == null)
                return;

            this.currentFilter = filter;
            // clone the base settings before applying changes otherwise we will loose the original configuration
            ColorGrading colorGradingCopy = Instantiate(baseSettings);
            profile.RemoveSettings<ColorGrading>();
            filter.ApplyChanges(colorGradingCopy);
            profile.AddSettings(colorGradingCopy);
            
        }

    }
}
