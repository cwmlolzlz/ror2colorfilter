﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace ColorFilter {

    public abstract class ColorFilterEffect: ScriptableObject {

        public string optionName;
        public string convarValue;

        public abstract void ApplyChanges(ColorGrading settings);
        
    }

    sealed class OffColorFilterOption : ColorFilterEffect {

        private void Awake() {
            this.optionName = "Off";
            this.convarValue = "off";
        }

        public override void ApplyChanges(ColorGrading settings) {
        }
    }

    sealed class SaturateColorFilterOption : ColorFilterEffect {

        private void Awake() {
            this.optionName = "Saturate";
            this.convarValue = "saturate";
        }

        public override void ApplyChanges(ColorGrading settings) {
            settings.saturation.Override(60f);
        }
    }

    sealed class Saturate2ColorFilterOption : ColorFilterEffect {

        private void Awake() {
            this.optionName = "Saturate+";
            this.convarValue = "saturate+";
        }

        public override void ApplyChanges(ColorGrading settings) {
            settings.saturation.Override(120f);
        }
    }

    sealed class SepiaColorFilterOption : ColorFilterEffect {

        private void Awake() {
            this.optionName = "Sepia";
            this.convarValue = "sepia";
        }

        public override void ApplyChanges(ColorGrading settings) {
            settings.mixerRedOutRedIn.Override(40f);
            settings.mixerRedOutGreenIn.Override(77f);
            settings.mixerRedOutBlueIn.Override(19f);

            settings.mixerGreenOutRedIn.Override(35f);
            settings.mixerGreenOutGreenIn.Override(70f);
            settings.mixerGreenOutBlueIn.Override(17f);

            settings.mixerBlueOutRedIn.Override(27f);
            settings.mixerBlueOutGreenIn.Override(53f);
            settings.mixerBlueOutBlueIn.Override(13f);
        }
    }

    sealed class NoirColorFilterOption : ColorFilterEffect {

        private void Awake() {
            this.optionName = "Noir";
            this.convarValue = "noir";
        }

        public override void ApplyChanges(ColorGrading settings) {
            settings.saturation.Override(-100); 

            settings.lift.Override(Vector4.one * -0.5f);
            settings.gain.Override(Vector4.one * 0.5f);
        }
    }

    sealed class GroovinSquidFilterOption : ColorFilterEffect {

        private void Awake() {
            this.optionName = "Groovin Squid";
            this.convarValue = "groovinSquid";
        }

        public override void ApplyChanges(ColorGrading settings) {
            settings.lift.Override(new Vector4(0.46410728f, 0.5652067f, 1f, 0f));
            settings.gamma.Override(new Vector4(1f, 0.52278465f, 0.9647942f, 0f));
            settings.gain.Override(new Vector4(1f, 0.41853583f, 0.54636717f, 0f));
        }
    }
}
