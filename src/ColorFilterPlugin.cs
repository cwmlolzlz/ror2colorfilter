using System.Runtime.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using BepInEx;
using R2API;
using R2API.Utils;
using RoR2.UI;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace ColorFilter {
    
    [BepInDependency ("com.bepis.r2api")]
    [BepInPlugin ("com.cwmlolzlz.colorfilter", "ColorFilter", "1.0.1")]

    [R2APISubmoduleDependency("CommandHelper")]
    public class ColorFilterPlugin : BaseUnityPlugin {
        
        public void Awake () {
            CommandHelper.AddToConsoleWhenReady();
            ColorFilterLoader.AddFiltersWhenReady();

            On.RoR2.RoR2Application.UnitySystemConsoleRedirector.Redirect += orig => { };

            On.RoR2.UI.SettingsPanelController.Start += (orig, self) => {
                orig(self);
                SetupColorblindOptions(self);
            };

            On.RoR2.SceneCamera.Awake += (orig, self) => {
                orig(self);
                PostProcessVolume postProcessVolume = self.GetComponentInChildren<PostProcessVolume>();
                ColorFilterController colorFilterController = self.gameObject.AddComponent<ColorFilterController>();
                colorFilterController.profile = postProcessVolume.profile;
            };
        }

        private void SetupColorblindOptions (SettingsPanelController settingsPanelController) {
            Debug.Log (settingsPanelController.gameObject.name);
            if (settingsPanelController.gameObject.name == "SettingsSubPanel, Video") {

                // create copy of HUD Scale option
                CarouselController hubCarouselController = Array.Find(settingsPanelController.GetComponentsInChildren<CarouselController> (), carouselController => {
                    return carouselController.gameObject.name.Contains("HUD");
                });

                if(hubCarouselController != null && hubCarouselController.gameObject != null) {
                    GameObject colorblindSettingsButtonGO = Instantiate(hubCarouselController.gameObject, hubCarouselController.gameObject.transform.parent);
                    colorblindSettingsButtonGO.name = "SettingsEntryButton, ColorFilter";
                    SetupColorblindOptions(colorblindSettingsButtonGO.GetComponent<CarouselController>());
                } else {
                    Debug.LogError("Failed to find a CarouselController to clone to display color filter options");
                }
                              
            }
        }

        private void SetupColorblindOptions(CarouselController carouselController) {
            if(carouselController == null) return;

            carouselController.settingName = ColorFilterConVar.instance.name;
            carouselController.settingSource = BaseSettingsControl.SettingSource.ConVar;
            carouselController.GetComponent<HGButton>().hoverToken = "Applies a color to the game world";
            carouselController.nameToken = "Color filter";
            carouselController.nameLabel.token = "Color filter";

            carouselController.choices = ColorFilterLoader.InstanceList.Select(filterOption => {
                var choice = new CarouselController.Choice();
                choice.suboptionDisplayToken = filterOption.optionName;
                choice.convarValue = filterOption.convarValue;
                choice.customSprite = null;
                return choice;
            }).ToArray();

            // triggers a OnEnable call that will revalidate the controls
            carouselController.enabled = false;
            carouselController.enabled = true;
        }

    }
}