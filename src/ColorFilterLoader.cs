﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace ColorFilter {

    public class ColorFilterLoader {

        public static IEnumerable<ColorFilterEffect> InstanceList {
            get { return m_InstanceList; }
        }

        private static readonly string OFF_RESERVED_VALUE = "off";

        private static SortedSet<ColorFilterEffect> m_InstanceList = new SortedSet<ColorFilterEffect>(Comparer<ColorFilterEffect>.Create( (lhs, rhs) => {
            if(lhs.convarValue == OFF_RESERVED_VALUE) {
                return -1;
            }

            if(rhs.convarValue == OFF_RESERVED_VALUE) {
                return 1;
            }

            string lhsAssemblyName = lhs.GetType().AssemblyQualifiedName;
            string rhsAssemblyName = rhs.GetType().AssemblyQualifiedName;

            if(lhsAssemblyName == typeof(ColorFilterEffect).AssemblyQualifiedName) {
                return -1;
            }

            if(rhsAssemblyName == typeof(ColorFilterEffect).AssemblyQualifiedName) {
                return 1;
            }

            if(lhsAssemblyName != rhsAssemblyName) {
                return String.Compare(lhsAssemblyName, rhsAssemblyName);
            }
            return String.Compare(lhs.convarValue, rhs.convarValue);
        }));

        internal static ColorFilterEffect GetOptionForValue(string convarValue) {
            return m_InstanceList.First(option => option.convarValue == convarValue);
        }

        public static void AddFiltersWhenReady() {
            Assembly assembly = Assembly.GetCallingAssembly();
            if(assembly == null) {
                return;
            }
            var baseType = typeof(ColorFilterEffect);
            var types = assembly.GetTypes().Where(t => t.IsSubclassOf(baseType));
            foreach(Type optionType in types) {
                ColorFilterEffect newOptionInstance = ScriptableObject.CreateInstance(optionType) as ColorFilterEffect;
                ColorFilterEffect existingOptionInstance = m_InstanceList.FirstOrDefault(option => option.convarValue == newOptionInstance.convarValue);
                if(newOptionInstance.convarValue == OFF_RESERVED_VALUE) {
                    Debug.LogErrorFormat("The color filter %s cannot have the convarValue %s", newOptionInstance.GetType().Name, OFF_RESERVED_VALUE);
                } else if(existingOptionInstance) {
                    Debug.LogErrorFormat("The color filter %s cannot be loaded. %s has already been registered with the convarValue %s", newOptionInstance.GetType().Name, existingOptionInstance.GetType().Name, existingOptionInstance.convarValue);
                } else {
                    m_InstanceList.Add(newOptionInstance);
                }
            }

            // if we failed to load the OffColorFilterOption then explicitly create it
            if(!m_InstanceList.Any(option => option.convarValue == OFF_RESERVED_VALUE)) {
                m_InstanceList.Add(ScriptableObject.CreateInstance<OffColorFilterOption>());
            }
        }
    }
}
