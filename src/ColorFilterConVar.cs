﻿using System;
using System.Collections.Generic;
using System.Text;
using RoR2;
using RoR2.ConVar;

namespace ColorFilter {

    internal class ColorFilterConVar {

        internal static StringConVar instance = new StringConVar(
            "colorfilter",
            ConVarFlags.Archive,
            "off",
            "Apples a fullscreen postprocessing color filter."
        );

        [RoR2.ConCommand(commandName = "colorfilter_set", flags = RoR2.ConVarFlags.None, helpText = "This is is a command and can take arguments.")]
        static void ColorFilterSet(RoR2.ConCommandArgs args) {
            args.CheckArgumentCount(1); //This cancels the command if there's not at least 2 arguments.
            args.GetArgString(0);
            ColorFilterConVar.instance.SetString(args[0]);
        }
    }
}
