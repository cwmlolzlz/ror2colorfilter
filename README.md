# Risk of Rain 2 - Color Filter Mod

## Description

This mod adds post processing filtering to the games world.
Changing the current filter is as easy as navigating to Video settings and clicking through to the option you want.

![Color filter option in video settings](https://gitlab.com/cwmlolzlz/ror2colorfilter/-/raw/master/Images/ror2colorfilter-menuoptions.gif)

There are some known times where the coloring is not applied:

* The game's UI. This is due to the game rendering it in a different stack, post processes cannot be applied at this point in time.
* Interaction highlights
* Approaching the Null portal in the Bazaar Between Time

## Available filters

### Groovin Squid

![Groovin Squid](https://gitlab.com/cwmlolzlz/ror2colorfilter/-/raw/master/Images/groovin_squid.jpg)

### Noir

![Noir](https://gitlab.com/cwmlolzlz/ror2colorfilter/-/raw/master/Images/noir.jpg)

### Saturate & Saturate+

![Saturate](https://gitlab.com/cwmlolzlz/ror2colorfilter/-/raw/master/Images/saturate.jpg)
![Saturate+](https://gitlab.com/cwmlolzlz/ror2colorfilter/-/raw/master/Images/saturate+.jpg)

### Sepia

![Sepia](https://gitlab.com/cwmlolzlz/ror2colorfilter/-/raw/master/Images/sepia.jpg)

## Making your own filter

As of version 1.0.1 this mod supports custom filters.
For a guide on creating your own filter try following [this guide](https://gitlab.com/cwmlolzlz/ror2colorfilter/-/blob/master/GUIDE.md).
You can also reach out to me on the community discord.
